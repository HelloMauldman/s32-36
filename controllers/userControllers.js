const User = require("../models/User")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

module.exports.checkEmail = (reqEmail) =>{
	return User.findOne({email:reqEmail.email}).then((result, error)=>{
		if(result !== null){
			return result;
		}
		else if(result == null){
			return (false);
		}
		else{
			return (error);
		}
	})
}


module.exports.register = (userInfo)=>{
	let newUser = new User(
		{
		firstName:userInfo.firstName,
		lastName: userInfo.lastName,
		email:userInfo.email,
		password: bcrypt.hashSync(userInfo.password, 10),
		mobileNo:userInfo.mobileNo,
		age:userInfo.age,
	})

	return newUser.save().then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}

module.exports.getAllUsers = ()=>{
	return User.find({}).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}


module.exports.login = (loginInfo)=>{
	return User.findOne({email: loginInfo.email}).then((result, error)=>{
		// console.log(result)
		if(result == null){
			console.log("email null.");
			return false;
		}
		else{
			let isPswdCorrect = bcrypt.compareSync(loginInfo.password, result.password);
			// console.log(loginInfo.password)
			// console.log(result.password)
			// console.log(isPswdCorrect)
			if(isPswdCorrect == true){
			//return json web token
				//invoke the functino which creates the token upon loggin in
				//requirements for creating a token:
					//if password matches from existing pw from db.
				return {access: auth.createsAccessToken(result)}
			}
			else{
				return "Incorrect Password";
		}
	}
})}

module.exports.getUserProfile = (userId) =>{
	return User.findById({_id: userId}).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}


module.exports.editProfile = (userId, userBody) =>{

	return User.findByIdAndUpdate(userId, userBody, {returnDocument:"after"}).then(result=> {

		return result
	})
}

module.exports.editUser = (userId, userBody)=>{
	// console.log(userId)
	// console.log(userBody.firstName)

	const updatedUser = {
		"firstName":userBody.firstName,
    	"lastName":userBody.lastName,
    	"email":userBody.email,
    	"password":bcrypt.hashSync(userBody.password, 10),
    	"mobileNo":userBody.mobileNo,
    	"age":userBody.age
	}
	return User.findByIdAndUpdate(userId, userBody, {returnDocumet: "after"});
}


module.exports.delete = (userId) => {
	return User.findByIdAndDelete(userId).then(result => {
		if(result){
			return true
		}
	})
}

module.exports.deleteEmail = (userEmail)=>{
	return User.findOneAndDelete(userEmail).then(result =>{
		if(result){
			return true;
		}
	})
}

module.exports.deleteEmailToken = (email) =>{
	// console.log(email);
	return User.findOneAndDelete(email).then(result =>{
		if(result){
			return true;
		}
	})
}

module.exports.enroll = async (data) => {
	const {userId, courseId} = data


	//look for matching document of a user
	const userEnroll = await User.findById(userId).then( (result, err) => {
		console.log(result)
		if(err){
			return err
		} else {
			// console.log(result)
			result.enrollments.push({courseId: courseId})

			return result.save().then( result => {
				return true
			})
		}

	})

	//look for matching document of a user
	const courseEnroll = await Course.findById(courseId).then( (result, err) => {
		if(err){
			return err
		} else {

			result.enrollees.push({userId: userId})

			return result.save().then( result => {
				return true
			})
		}
	})

	//to return only one value for the function enroll

	if(userEnroll && courseEnroll){
		return true
	} else {
		return false
	}
}