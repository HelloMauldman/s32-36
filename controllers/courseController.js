const Course = require("../models/Course")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

module.exports.getAllCourses = ()=>{
	return Course.find().then((result, error)=>{
		if(error){
			return false
		}
		else{
			return result
		}
	})
}

//createCourse


module.exports.createCourse = (courseInfo) =>{
	let newCourse = new Course({
		courseName: courseInfo.courseName,
		description: courseInfo.description,
		price: courseInfo.price
	})
	return newCourse.save().then((savedCourse, err)=>{
		if(err){
			return false;
		}
		else{
			return savedCourse;
		}
	})
}

//getActiveCourse

module.exports.getActiveCourses = ()=>{
	return Course.find({isActive:true}).then((result,error)=>{
		if(error){
			return false;
		}
		else{
			return result
		}
	})
}

//getSpecificCourse

module.exports.getSpecificCourse = (courseName) =>{
	return Course.findOne({courseName: courseName}).then((result, error)=>{
		if(result !== null){
			return result
		}
		else if(result == null){
			return false
		}
		else{
			return console.log(error)
		}
	})
}