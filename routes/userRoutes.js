const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers")
const auth = require("../auth.js")


// router.post("/email-exists",(req, res)=>{
// 	return User.findOne({email:req.body.email}).then((result, error)=>{
// 		if(result !== null){
// 			res.send(false);
// 		}
// 		else if(result == null){
// 			res.send(true);
// 		}
// 		else{
// 			res.send(error);
// 		}

// 		// console.log(result)
// 	})
// })

router.post("/email-exists",(req, res)=>{
	userController.checkEmail(req.body).then(result => res.send(result))
})

router.post("/register",(req, res)=>{
	userController.register(req.body).then(result=>{res.send(result)})
})

router.get("/", (req, res)=>{
	userController.getAllUsers().then(result=>res.send(result))
})

router.post("/login", (req, res)=>{
	userController.login(req.body).then(result=>res.send(result))
})

router.get("/details", auth.verify, (req, res)=>{
	// console.log(req.headers.authorization)
	let userData = auth.decode(req.headers.authorization)
	res.send(userData)
})

router.get("/:id", (req, res)=>{
	userController.getUserProfile(req.params.id).then(result => res.send(result))
})


//edit user information
router.put("/edit", auth.verify, (req, res)=>{
	
	const userId = auth.decode(req.headers.authorization).payload.id

	// console.log(userId)
	userController.editUser(userId, req.body).then(result => res.send(result))
})

router.delete("/:id/delete", (req, res)=>{
	userController.delete(req.params.id).then(result => res.send(result))
})

router.delete("/delete", (req, res)=>{
	userController.deleteEmail(req.body.email).then(result => res.send(result))
})

router.delete("/delete-email", (req, res)=>{
	const email = auth.decode(req.headers.authorization).email;
	// console.log(email)

	// userController.deleteEmailToken(email).then(result =>{res.send(result)})
})

router.post("/enroll", auth.verify, (req, res)=>{
	
	let data ={
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(result =>{res.send(result)})
})


//mini activity


module.exports =  router;