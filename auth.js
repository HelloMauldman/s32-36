//json web token
	//sign- function thatt creates a token 
	//verify -  functions that checcks if  there's presence of token
	//decode - function that decodes the token

const jwt = require("jsonwebtoken");
const secret = "gwapoAko"

module.exports.createsAccessToken = (user) => {
	//user = object because it came from the matching document of the user from the database
	//jwt.sign(payload, secret, {});
	const payload = {
		id:user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(payload, secret,  {});
}

module.exports.decode = (token)=>{
	let slicedToken = token.slice(7, token.length);
	console.log(slicedToken)
	return jwt.decode(slicedToken,{complete: true})
}

module.exports.verify = (req, res, next) =>{
	// console.log(req.headers.authorization)
	let token = req.headers.authorization;
	
	// console.log(token)
	if(typeof token !== "undefined"){
		let slicedToken = token.slice(7, token.length);
		return jwt.verify(slicedToken, secret, (err, data)=>{
			if(err){
				return res.send({auth: "failed"})
			}
			else{
				next();
			}
		})
	}
	else{
		res.send(false);
	}
}